title: "Čerpání eurodotací: Česko ve špičce mezi hříšníky, drtivá většina nesrovnalostí se k soudu nedostane"
perex: "Evropský Úřad pro boj proti podvodům (OLAF) eviduje v České republice ročně desítky &bdquo;podvodných nesrovnalostí&ldquo; u projektů v hodnotě stovek milionů korun. Jen u několika málo případů se daří podvody prokázat a pachatele postavit před soud."
authors: ["Petr Kočí"]
published: "8. září 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/uploader/olaf_170906-180815_pek.jpg
coverimg_note: "Vyšetřovatelé Evropského úřadu pro boj proti podvodům | Foto <a href='https://twitter.com/OLAFPress/status/860840609770332161'>OLAF</a>"
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1", "https://interaktivni.rozhlas.cz/tools/highcharts/5.0.9.min.js"]
options: "" #wide
---

I vyšetřování dotace na Čapí hnízdo začalo jako pouhá *nesrovnalost*. „V našem systému (…) k případu Čapího hnízda není nic. V MSC2007 jsem dohledala pouze nesrovnalost z roku 2011,“ napsala v e-mailu, který loni [zveřejnil časopis Reportér](http://reportermagazin.cz/a/iKdpU/za-babise-za-agrofert), referentka ministerstva financí Barbora Boschat. Pro svého nadřízeného měla v databázích potají zjistit, jak se rozjíždí vyšetřování dotačního podvodu kolem Čapího hnízda. Právě kvůli němu poslanci ve středu vydali policii k trestnímu stíhání šéfa ANO Andreje Babiše a jeho pravou ruku Jaroslava Faltýnka.

Slovo *nesrovnalost* vypadá nenápadně – pro experty, kteří hlídají, zda příjemci dotací utrácejí evropské peníze poctivě, má ovšem významy, které nemusejí být obecně známé.

## Od opomenutí po trestný čin

*Nesrovnalosti* [jsou](http://www.mfcr.cz/assets/cs/media/Metodika_Metodicky-pokyn-Hlaseni-nesrovnalosti-OLAF-2014-2020.pdf) „porušení právních předpisů, která vedou nebo by mohla vést ke ztrátě v souhrnném rozpočtu EU nebo ve veřejném rozpočtu ČR“.

[Patří mezi ně](https://www.strukturalni-fondy.cz/getmedia/647cdcb0-1fea-4c8f-8c41-86a5aa6e6fe8/6_Seznam-druhu-nesrovnalosti-vcetne-jejich-kodoveh_647cdcb0-1fea-4c8f-8c41-86a5aa6e6fe8.docx?ext=.docx) zdánlivé drobnosti jako například nepřesnosti v účetnictví či nedodržení lhůt, ale také falšování dokumentů, podplácení, vykázání fiktivního pozemku či případy, kdy se skutečný příjemce dotace vydává za někoho jiného, aby formálně  splnil podmínky pro její přidělení.

Aby drobná administrativní opomenutí oddělili od vážných případů, rozlišují hlídači eurofondů běžné nesrovnalosti a *nesrovnalosti podvodného charakteru*. Hlavní rozdíl mezi nimi je úmyslné jednání.

<div id="graf1" style="width:100%; height:400px"></div>
<hr>

Běžných nesrovnalostí české úřady [loni nahlásily víc než pět set](https://ec.europa.eu/anti-fraud/sites/antifraud/files/1_act_part1_v2_en.pdf), týkaly se projektů celkem za 3,8 miliardy korun.

Oproti minulým letům nejde o nijak výjimečný počet případů. Zatím nejvíce běžných nesrovnalostí napočítali úředníci v roce 2013, kdy Česko evropskému žebříčku finančních přešlapů kralovalo. Tehdy šlo o 1060 nesrovnalostí u projektů za 359 milionů eur, tedy za 9,3 miliardy korun.

Na vině byl pravděpodobně spěch kvůli končícímu dotačnímu období 2007 - 2013 a také nedostatek kvalifikovaných úředníků. Nesrovnalosti se totiž neobjevují jen u příjemců dotací, ale také u jejich administrátorů.

Jak je vidět z grafů, nesrovnalosti podvodného charakteru, tedy ty úmyslné, tvoří ze všech českými úřady nahlášených nesrovnalostí jen malý zlomek.

<div id="graf2" style="width:100%; height:400px"></div>
<hr>

Evropská komise varuje před zjednodušenou interpretací, podle které vysoký počet podvodných nesrovnalostí ukazuje automaticky na země, v nichž se s eurofondy nejvíc podvádí: "Počet nahlášených nesrovnalostí podvodného charakteru měří výsledky, jichž dosahují členské státy v boji proti podvodům a dalším protiprávním činnostem, které poškozují finanční zájmy EU. Neměl by být vykládán jako míra podvodovosti na území daného členského státu," píše se v každé [výroční zprávě Evropské komise o boji proti podvodům](https://ec.europa.eu/anti-fraud/reports_en). 

Většinu nesrovnalostí sice tvoří právě různá administrativní opomenutí, například chybné datum na některé z předložených listin. I taková zdánlivá drobnost může ovšem vést k odhalení podvodu: [Analýza](https://ec.europa.eu/anti-fraud/sites/antifraud/files/1_autre_document_travail_service_part1_v2_en.pdf), kterou Evropská komise v červenci předložila Evropskému parlamentu. uvádí příklad z Chorvatska: zemědělské družstvo tam při žádosti o dotaci z eurofondů mělo jen o měsíc posunuté datum na potvrzení o daňové bezdlužnosti. Jen díky němu vyšetřovatelé [Evropského úřadu pro boj proti podvodům (známého pod zkratkou OLAF)](https://ec.europa.eu/anti-fraud//home_en) zjistili, že celý dokument je zfalšovaný a případ skončil odsouzením padělatele a vrácením dotace.

## Česko na čtvrtém místě v podvodech

Jak moc se tedy v Česku s eurodotacemi podvádí a jak se úřadům daří podezřelé transakce identifikovat? Protože přidělování dotací a vyšetřování eventuálních podvodů trvá měsíce a roky, mezinárodní srovnání dává zatím smysl jen u skončeného [programového období 2007 - 2013](https://www.strukturalni-fondy.cz/cs/Fondy-EU/Programove-obdobi-2007-2013).

Čeští příjemci v něm z evropských strukturálních fondů získali 25,8 miliardy eur (přibližně 670 miliard korun). Z nich byla zhruba každá dvacátá proplacená koruna zatížena běžnými nesrovnalostmi. Přesně 88 haléřů z každé stokoruny pak připadá na nesrovnalosti podvodného charakteru.

V podílu nahlášených nesrovnalostí podvodného charakteru je Česko v EU na čtvrtém místě za Slovenskem, Itálií a Lotyšskem. U běžných nesrovnalostí je druhé hned za Slovenskem.

<wide><h3>Kolik z proplacených dotací v období 2007 - 2013 bylo zatíženo nesrovnalostmi podvodného charakteru</h3><div class="table__swipe hide--d"><span class="icon-svg icon-svg--swipe-left"><svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="/sites/all/themes/custom/irozhlas/img/bg/icons-svg.svg#icon-swipe-left" x="0" y="0" width="100%" height="100%"></use></svg></span></div><table class="table table--striped-red table--plain table--responsive table--w100p"><thead><tr><th></th><th></th><th>nesrovnalostí</th><th>objem nesrovnalostí v eurech</th><th>celkově proplaceno v eurech</th><th>podíl nesrovnalostí</th></tr></thead><tbody><tr><td>1.</td><td>Slovensko</td><td>165</td><td>140 104 764</td><td>11 493 455 618</td><td style="font-weight: bold;">1,22 %</td></tr><tr><td >2.</td><td>Itálie</td><td>101</td><td>306 791 907</td><td>28 288 501 684</td><td style="font-weight: bold;">1,08 %</td></tr><tr><td>3.</td><td>Lotyšsko</td><td>89</td><td>41 964 616</td><td>4 655 284 168</td><td style="font-weight: bold;">0,90 %</td></tr><tr style="background:red"><td >4.</td><td>Česká republika</td><td>149</td><td>228 389 445</td><td>25 843 724 870</td><td style="font-weight: bold;">0,88 %</td></tr><tr><td>5.</td><td>Rumunsko</td><td>237</td><td>140 772 095</td><td>19 185 442 449</td><td style="font-weight: bold;">0,73 %</td></tr><tr><td>6.</td><td>Slovinsko</td><td>27</td><td>26 233 107</td><td>4 121 031 332</td><td style="font-weight: bold;">0,64 %</td></tr><tr><td>7.</td><td>Polsko</td><td>276</td><td>396 116 814</td><td>67 894 066 494</td><td style="font-weight: bold;">0,58 %</td></tr><tr><td>8.</td><td>Portugalsko</td><td>47</td><td>90 392 248</td><td>21 620 834 490</td><td style="font-weight: bold;">0,42 %</td></tr><tr><td>9.</td><td>Chorvatsko</td><td>4</td><td>2 277 409</td><td>866 162 519</td><td style="font-weight: bold;">0,26 %</td></tr><tr><td>10.</td><td>Nizozemsko</td><td>12</td><td>3 903 370</td><td>1 699 962 581</td><td style="font-weight: bold;">0,23 %</td></tr><tr><td>11.</td><td>Estonsko</td><td>22</td><td>7 807 152</td><td>3 486 691 600</td><td style="font-weight: bold;">0,22 %</td></tr><tr><td>12.</td><td>Belgie</td><td>6</td><td>3 238 250</td><td>2 084 119 208</td><td style="font-weight: bold;">0,16 %</td></tr><tr><td>13.</td><td>Bulharsko</td><td>42</td><td>8 447 298</td><td>6 652 736 814</td><td style="font-weight: bold;">0,13 %</td></tr><tr><td>14.</td><td>Německo</td><td>222</td><td>32 617 788</td><td>25 566 221 638</td><td style="font-weight: bold;">0,13 %</td></tr><tr><td>15.</td><td>Británie</td><td>48</td><td>11 546 212</td><td>10 001 559 591</td><td style="font-weight: bold;">0,12 %</td></tr><tr><td>16.</td><td>Rakousko</td><td>7</td><td>1 155 265</td><td>1 175 404 280</td><td style="font-weight: bold;">0,10 %</td></tr><tr><td>17.</td><td>Kypr</td><td>7</td><td>532 224</td><td>631 910 587</td><td style="font-weight: bold;">0,08 %</td></tr><tr><td>18.</td><td>Řecko</td><td>45</td><td>11 779 140</td><td>20 357 050 909</td><td style="font-weight: bold;">0,06 %</td></tr><tr><td>19.</td><td>Španelsko</td><td>118</td><td>18 771 459</td><td>35 427 318 471</td><td style="font-weight: bold;">0,05 %</td></tr><tr><td>20.</td><td>Dánsko</td><td>1</td><td>201 898</td><td>631 909 007</td><td style="font-weight: bold;">0,03 %</td></tr><tr><td>21.</td><td>Maďarsko</td><td>83</td><td>7 179 221</td><td>24 927 535 329</td><td style="font-weight: bold;">0,03 %</td></tr><tr><td>22.</td><td>Litva</td><td>14</td><td>1 818 634</td><td>6 829 310 612</td><td style="font-weight: bold;">0,03 %</td></tr><tr><td>23.</td><td>Malta</td><td>15</td><td>266 825</td><td>848 158 961</td><td style="font-weight: bold;">0,03 %</td></tr><tr><td>24.</td><td>Francie</td><td>6</td><td>2 886 409</td><td>13 723 561 554</td><td style="font-weight: bold;">0,02 %</td></tr><tr><td style="font-weight: bold;">25.</td><td>Finsko</td><td>1</td><td>179 375</td><td>1 633 737 555</td><td style="font-weight: bold;">0,01 %</td></tr><tr><td>26.</td><td>Irsko</td><td>2</td><td>15 672</td><td>791 480 398</td><td style="font-weight: bold;">0,00 %</td></tr><tr><td>27.</td><td>Lucembursko</td><td>0</td><td>0</td><td>50 487 332</td><td style="font-weight: bold;">0,00 %</td></tr><tr><td>28.</td><td>Švédsko</td><td>4</td><td>66 797</td><td>1 674 073 781</td><td style="font-weight: bold;">0,00 %</td></tr></tbody></table><div style="width:100%;text-align: right">Zdroj: [Evropská komise](https://ec.europa.eu/anti-fraud/sites/antifraud/files/1_autre_document_travail_service_part2_v2_en.pdf)</div></wide>

Jen u jednotek případů se daří podvody prokázat a pachatele postavit před soud. Z několika stovek *podvodných nesrovnalostí* zjištěných od roku 2009 eviduje OLAF (podle jeho vyšetřovatelů) prokázaných podezření na dotační podvody v České republice deset.

"Omezené pravomoce a praktické možnosti našich vyšetřovatelů někdy navzdory jejich velké snaze neumožní shromáždit dostatek důkazů pro trestní řízení," [vysvětluje](https://ec.europa.eu/anti-fraud/sites/antifraud/files/olaf_report_2016_en.pdf) nízká čísla úřad pro boj proti podvodům.

## Znovu od nuly
Když už evropští vyšetřovatelé dojdou k závěru, že jde skutečně o podvod, vydají tuzemským orgánům doporučení k zahájení trestního stíhání a k vymáhání podvodně vyplacené dotace. Takové doporučení však nemá v českém právním prostředí o mnoho větší váhu než běžné trestní oznámeni.

Podle poslední [výroční zprávy OLAFu](https://ec.europa.eu/anti-fraud/sites/antifraud/files/olaf_report_2016_en.pdf) spadla zatím většina těchto doporučení pod stůl. Mezi lety 2009 a 2016 čeští vyšetřovatelé a státní zástupci tři doporučení prověřili, ale nezahájili trestní stíhání. Ve čtyřech případech trestní stíhání přerušili a jen dva případy dospěly do stádia obžaloby.

Zatímco v celé Evropské unii skončí před soudem každé druhé doporučení OLAFu, v Česku je to to jen každé třetí, ve sledovaném období tedy přesně dva ze šesti.

Protože české právo neumožňuje použít uzavřené vyšetřování OLAFu jako důkaz, musejí čeští vyšetřovatelé a státní zástupci každé podezření vyšetřit od začátku a znovu shromáždit všechny důkazy. Průměrné vyšetřování OLAFu trvá jeden až dva roky, pokud se české úřady rozhodnou na základě jeho doporučení zahájit trestní stíhání, začínají znovu od nuly a celý proces se protáhne o další roky.

"Schopnost služby kriminální policie a vyšetřování je v některých svých oblastech zejména v odhalování a vyšetřování hospodářské trestné činnosti v rámci trestního řízení, výrazně omezena," píše se [v koncepci rozvoje policie České republiky v letech 2016 - 2020](http://www.ceskatelevize.cz/ct24/sites/default/files/1757756-policie-i-1.pdf). 

"Se stávajícím počtem policistů, kteří se zabývají touto problematikou, nelze obsáhnout celou šíři páchání této trestné činnosti v reálném čase a v potřebné kvalitě. Již v současné době neumožňuje početní stav policistů zařazených na problematice odhalování a vyšetřování trestné činnosti hospodářského charakteru žádnou další činnost, mimo oblast zpracování ohlášených podání. I tato podání jsou zpracovávána ve lhůtách, které nekorespondují se lhůtami uloženými zákonem, zejména pak zákonem o trestním řízení soudním."