Highcharts.chart('graf2', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Nesrovnalosti nahlášené z ČR v milionech eur'
    },
    exporting: {
            enabled: false    
    },
    credits: {
        enabled: true,
        text: 'Zdroj: Ochrana finančních zájmů Evropské unie - výroční zprávy za roky 2012-2016 o boji proti podvodům',
        href: 'https://ec.europa.eu/anti-fraud/reports_en'
    },
    lang: {
        decimalPoint: ',',
        thousandsSep: ' '
    },
    xAxis: {
        categories: ['2012', '2013', '2014', '2015', '2016']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'hodnota projektů v milionech eur'
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y:.1f} milionů EUR<br/>Celkem: {point.stackTotal:.1f} milionů EUR'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
                format: '{y:.0f}'
            }
        }
    },
    series: [{
        name: 'podvodného charakteru',
        data: [54.478847, 13.388827, 36.668091, 15.190237, 31.319598],
        color: "red"
    }, {
        name: 'nepodvodného charakteru',
        data: [1036.156953, 359.076203, 293.508822, 236.946526, 115.079639],
        color: "lightgrey"
    }]
});