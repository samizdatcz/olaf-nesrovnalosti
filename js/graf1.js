Highcharts.chart('graf1', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Nesrovnalosti nahlášené Českou republikou'
    },
    exporting: {
            enabled: false    
    },
    credits: {
        enabled: true,
        text: 'Zdroj: Ochrana finančních zájmů Evropské unie - výroční zprávy za roky 2012-2016 o boji proti podvodům',
        href: 'https://ec.europa.eu/anti-fraud/reports_en'
    },
    xAxis: {
        categories: ['2012', '2013', '2014', '2015', '2016']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'počet nesrovnalostí'
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Celkem: {point.stackTotal}'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black'
            }
        }
    },
    series: [{
        name: 'podvodného charakteru',
        data: [36, 35, 42, 48, 51],
        color: "red"
    }, {
        name: 'nepodvodného charakteru',
        data: [657, 1060, 1032, 680, 456],
        color: "lightgrey"
    }]
});